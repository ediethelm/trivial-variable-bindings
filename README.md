# Trivial Variable Bindings Manual

###### \[in package TRIVIAL-VARIABLE-BINDINGS\]
[![pipeline status](https://gitlab.com/ediethelm/trivial-variable-bindings/badges/master/pipeline.svg)](https://gitlab.com/ediethelm/trivial-variable-bindings/commits/master)
[![Quicklisp](http://quickdocs.org/badge/trivial-variable-bindings.svg)](http://quickdocs.org/trivial-variable-bindings/)
[![coverage report](https://gitlab.com/ediethelm/trivial-variable-bindings/badges/master/coverage.svg?job=test-coverage)](https://gitlab.com/ediethelm/trivial-variable-bindings/-/jobs/artifacts/master/browse?job=test-coverage)

## Description

This simple library offers a way to handle associations between a place-holder (aka. variable) and a value. See *trivial-filter* for an application.

## Installing trivial-variable-bindings

This project is available in the latest [QuickLisp](https://www.quicklisp.org/beta/ "QuickLisp") distribution, so installing it is reduced to calling:

```lisp
(ql:quickload :trivial-variable-bindings)
```


## Working Example

See *trivial-filter* for an application.

## Exported Symbols

- [class] PLACE-HOLDER

    A named variable.

- [method] PLACE-HOLDER-NAME (PLACE-HOLDER PLACE-HOLDER)
   
   Reader of the name of a *place-holder*.

- [class] VARIABLE-BINDING

    The binding of a named variable to a specific value.

- [method] BOUND-VARIABLE (VARIABLE-BINDING VARIABLE-BINDING)
  
    Reader of the *place-holder* bound.

- [method] BOUND-VALUE (VARIABLE-BINDING VARIABLE-BINDING)

    Accessor of the value a *place-holder* is bound to.
	
- [class] BINDINGS

    A set of named variabled bound to corresponding values.

- [method] BOUND-VARIABLES (BINDINGS BINDINGS)

    Accessor to the list of all *variable-binding*s contained in this *bindings*. 

- [function] CONTAINS VAR BINDINGS

    Verify that a *place-holder* **VAR** is bound to a specific value in the *bindings* container **BINDINGS**.

- [function] BOUND-VARIABLE-VALUE VAR BINDINGS

    Return the value associated to the *place-holder* **VAR** in the *bindings* container **BINDINGS**, if any. Otherwise return the *place-holder* **VAR** itself.

- [function] REMOVE-BOUND-VARIABLE VAR BINDINGS

    Delete the bound *place-holder* **VAR** in the *bindings* container **BINDINGS**.

- [method] EQUALS (LHS PLACE-HOLDER) (RHS PLACE-HOLDER)

    Compares two place-holder for equality. Only equally named place-holders are equal.

- [method] EQUALS (LHS BOUND-VARIABLE) (RHS BOUND-VARIABLE)

    Compares two bound-variables for equality.

- [method] EQUALS (LHS BINDINGS) (RHS BINDINGS)

    Compares two bindings for equality.

- [method] CLONE (BINDINGS BINDINGS)

    Make a deep copy of the given *bindings* container **BINDINGS**.


## License Information

This library is released under the MIT License. Please refer to the [LICENSE](https://gitlab.com/ediethelm/trivial-variable-bindings/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/ediethelm/trivial-variable-bindings/blob/master/CONTRIBUTING.md "Contributing") document for more information.
