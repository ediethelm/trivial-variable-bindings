;;;; Copyright (c) Eric Diethelm 2018 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license.

(in-package :trivial-variable-bindings)

(5am:def-suite :trivial-variable-bindings-tests :description "Trivial Variable Bindings tests")
(5am:in-suite :trivial-variable-bindings-tests)

(5am:test place-holder
  (let ((?X1 (make-instance 'place-holder :name "X"))
	(?X2 (make-instance 'place-holder :name "X"))
	(?Y (make-instance 'place-holder :name "Y")))

    (5am:is-true (equals ?X1 ?X1))
    (5am:is-false (equals ?X1 ?Y))
    (5am:is-true (equals ?X1 ?X2))))


(5am:test variable-binding
  (let* ((?X1 (make-instance 'place-holder :name "X"))
	 (?X2 (make-instance 'place-holder :name "X"))
	 (?Y (make-instance 'place-holder :name "Y"))
	 (obj1 :a)
	 (obj2 :b)
	 (bound-variable1 (make-instance 'variable-binding :bound-variable ?X1 :bound-value obj1))
	 (bound-variable2 (make-instance 'variable-binding :bound-variable ?X2 :bound-value obj1))
	 (bound-variable3 (make-instance 'variable-binding :bound-variable ?X2 :bound-value obj2))
	 (bound-variable4 (make-instance 'variable-binding :bound-variable ?Y :bound-value obj1)))

    (5am:is-true (equals bound-variable1 bound-variable2))
    (5am:is-false (equals bound-variable2 bound-variable3))
    (5am:is-false (equals bound-variable1 bound-variable4))))



(5am:test bindings
  (let* ((?X1 (make-instance 'place-holder :name "X"))
	 (?X2 (make-instance 'place-holder :name "X"))
	 (?Y (make-instance 'place-holder :name "Y"))
	 (obj1 :a)
	 (obj2 :b)
	 (bindings1 (make-instance 'bindings))
	 (bindings2 (make-instance 'bindings)))


    (5am:is-true (equals bindings1 bindings2))
    
    (setf (bound-variable-value ?X1 bindings1) obj1)
    (5am:is-false (equals bindings1 bindings2))
    (5am:is (eq (bound-variable-value ?X1 bindings1) obj1))
    
    (setf (bound-variable-value ?X2 bindings2) obj1)
    (5am:is-true (equals bindings1 bindings2))
    (5am:is (eq (bound-variable-value ?X1 bindings1) obj1))
    (5am:is (eq (bound-variable-value ?X2 bindings2) obj1))
    
    (setf (bound-variable-value ?Y bindings2) obj2)
    (5am:is-false (equals bindings1 bindings2))))
