;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-


(defsystem :trivial-variable-bindings
  :name "trivial-variable-bindings"
  :description "Offers a way to handle associations between a place-holder (aka. variable) and a value."
  :version "0.1.7"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-utilities
	       :iterate)
  :in-order-to ((test-op (test-op :trivial-variable-bindings/test)))
  :components ((:file "package")
	       (:file "trivial-variable-bindings")))

(defsystem :trivial-variable-bindings/test
  :name "trivial-variable-bindings/test"
  :description "Unit Tests for the trivial-variable-bindings project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-variable-bindings fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :trivial-variable-bindings-tests))
  :components ((:file "test-trivial-variable-bindings")))

