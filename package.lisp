(uiop:define-package #:trivial-variable-bindings
  (:documentation "")
  (:use #:common-lisp)
  (:import-from :trivial-utilities #:equals #:clone)
  (:export #:place-holder
	   #:place-holder-name
	   #:variable-binding
	   #:bound-variable
	   #:bound-value
	   #:bindings
	   #:bound-variables
	   #:bound-variable-value
	   #:remove-bound-variable
	   #:contains))

