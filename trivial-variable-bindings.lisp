;;;; Copyright (c) Eric Diethelm 2018 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license.

(in-package :trivial-variable-bindings)


(defclass place-holder ()
  ((name :reader place-holder-name
	 :initarg :name))
  (:documentation "A named variable."))

(defclass variable-binding ()
  ((variable :reader bound-variable
	     :initarg :bound-variable
	     :type place-holder)
   (value :accessor bound-value
	  :initarg :bound-value
	  :type t))
  (:documentation "The binding of a named variable to a specific value."))

(defclass bindings ()
  ((bound-variables :accessor bound-variables
		    :initarg :bound-variables
		    :initform nil
		    :type list))
  (:documentation "A set of named variabled bound to corresponding values."))


;;--------------------- Equality checks ---------------------

(defmethod equals ((lhs place-holder) (rhs place-holder) &key &allow-other-keys)
  "Compares two place-holder for equality. Only equally named place-holders are equal."
  (equals (place-holder-name lhs) (place-holder-name rhs)))

(defmethod equals ((lhs variable-binding) (rhs variable-binding) &key &allow-other-keys)
  "Compares two bound-variables for equality."
  (and (equals (bound-variable lhs) (bound-variable rhs))
       (equals (bound-value lhs) (bound-value rhs))))

(defmethod equals ((lhs bindings) (rhs bindings) &key &allow-other-keys)
  "Compares two bindings for equality."
  (and (eq (length (bound-variables lhs)) (length (bound-variables rhs)))
       (notany #'(lambda (x y) (not (equals x y))) (bound-variables lhs) (bound-variables rhs))))


;;--------------------- Pretty printing ---------------------

(defmethod print-object ((var place-holder) out)
  (print-unreadable-object (var out :type nil)
    (format out ":?~A" (place-holder-name var))))

(defmethod print-object ((var variable-binding) out)
  (print-unreadable-object (var out :type nil)
    (format out "~A: ~A" (bound-variable var) (bound-value var))))

(defmethod print-object ((var bindings) out)
  (print-unreadable-object (var out :type nil)
    (format out "Bindings (")
    (mapcar #'(lambda (x) (print-object x out)) (bound-variables var))
    (format out ")")))

;;---------------------  ---------------------

(defun contains (var bindings)
  "Verify that a *place-holder* **VAR** is bound to a specific value in the *bindings* container **BINDINGS**."
  (declare (type bindings bindings))
  (the boolean
       (if (member var (bound-variables bindings) :key #'bound-variable :test #'equals)
	   t
	   nil)))

(defun bound-variable-value (var bindings)
  "Return the value associated to the *place-holder* **VAR** in the *bindings* container **BINDINGS**, if any. Otherwise return the *place-holder* **VAR** itself."
  (declare (type place-holder var)
	   (type bindings bindings))
  (trivial-utilities:aif (car (member var (bound-variables bindings) :key #'bound-variable :test #'equals))
			 (bound-value it)
			 var))

(defun (setf bound-variable-value) (value var bindings)
  ""
  (declare (type place-holder var)
	   (type bindings bindings))
  (trivial-utilities:aif (car (member var (bound-variables bindings) :key #'bound-variable :test #'equals))
			 (setf (bound-value it) value)
			 (push (make-instance 'variable-binding :bound-variable var :bound-value value) (bound-variables bindings))))

(defun remove-bound-variable (var bindings)
  "Delete the bound *place-holder* **VAR** in the *bindings* container **BINDINGS**."
  (setf (bound-variables bindings) (delete var (bound-variables bindings) :key #'bound-variable :test #'equals)))

(defmethod clone ((bindings bindings) &key &allow-other-keys)
  "Make a deep copy of the given *bindings* container **BINDINGS**."
  (make-instance 'bindings :bound-variables (copy-list (bound-variables bindings))))
